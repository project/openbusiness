<?php

namespace Drupal\Tests\openbusiness_profile\Functional;

use Drupal\KernelTests\AssertConfigTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for OpenBusiness profile.
 *
 * @group openbusiness_profile
 */
class OpenBusinessProfileTest extends BrowserTestBase {
  use AssertConfigTrait;

  /**
   * Set the profile to install as a basis.
   *
   * {@inheritdoc}
   */
  protected $profile = 'openbusiness_profile';

  /**
   * {@inheritdoc}
   */
  protected function installProfile() {
    $parameters = parent::installProfile();
    $parameters['forms']['install_configure_form']['site_mail'] = 'test@openbusiness.com';
    return $parameters;
  }

  /**
   * Set config schema to false beacause metatag module doesn't have one.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    // Uninstall recaptcha to allow tests to log in the created users.
    $this->container->get('module_installer')->uninstall(['recaptcha']);
  }

  /**
   * Test if the profile has the openbusiness_theme on the appearance page.
   */
  public function testAppearance() {
    $account = $this->drupalCreateUser(['administer themes']);
    $this->drupalLogin($account);

    $this->drupalGet('admin/appearance');
    $this->assertSession()->pageTextContains('OpenBusiness');
  }

  /**
   * Test if the admin is able to create nodes.
   */
  public function testAddNode() {
    // Create an user with permission.
    $account = $this->drupalCreateUser(['create article content']);
    $this->drupalLogin($account);

    $this->drupalCreateNode(['title' => $this->t('Create Blog Node'), 'type' => 'article']);
    $this->drupalGet('/blog');
    $this->assertSession()->pageTextContains('Create Blog Node');
  }

  /**
   * Test if the admin is able to edit nodes.
   */
  public function testEditNodesByAdmin() {
    $account = $this->drupalCreateUser(['edit any page content']);
    $this->drupalLogin($account);
    $webassert = $this->assertSession();

    $nodes = $this->container->get('entity_type.manager')
      ->getStorage('node')
      ->loadByProperties(['title' => 'Mission']);
    $node = reset($nodes);
    $this->drupalGet($node->toUrl('edit-form'));
    $webassert->statusCodeEquals('200');
    $this->submitForm([], "Save");
    $webassert->pageTextContains('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
  }

}
