<?php

namespace Drupal\openbusiness_content_creator;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\path_alias\AliasManager;
use Drupal\Core\State\StateInterface;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a helper class for importing default content.
 *
 * @internal
 *   This code is only for use by the Openbusiness profile: Content Creator.
 */
class InstallCreatorContent implements ContainerInjectionInterface {

  /**
   * The path alias manager.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * State.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new InstallHelper object.
   *
   * @param \Drupal\path_alias\AliasManager $aliasManager
   *   The path alias manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   */
  public function __construct(AliasManager $aliasManager, EntityTypeManagerInterface $entityTypeManager, ModuleHandlerInterface $moduleHandler, StateInterface $state, FileSystemInterface $fileSystem) {
    $this->aliasManager = $aliasManager;
    $this->moduleHandler = $moduleHandler;
    $this->entityTypeManager = $entityTypeManager;
    $this->state = $state;
    $this->fileSystem = $fileSystem;
  }

  /**
   * Instantiates a new instance of this class.
   *
   * This is a factory method that returns a new instance of this class. The
   * factory should pass any needed dependencies into the constructor of this
   * class, but not the container itself. Every call to this method must return
   * a new instance of this class; that is, it may not implement a singleton.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this instance should use.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('path_alias.manager'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('state'),
      $container->get('file_system')
    );
  }

  /**
   * Getting path for module.
   */
  protected function getModulePath($entity_type, $bundle_machine_name)
  {
      return $this->moduleHandler->getModule('openbusiness_content_creator')
        ->getPath() . '/content/' . $entity_type . '/' . $bundle_machine_name . '.csv';
  }

  /**
   * Getting path for module.
   */
  protected function getImagesPath($image_name) {
    $ext = pathinfo($image_name, PATHINFO_EXTENSION);
    if ($ext === 'png') {
      $image_path = $this->moduleHandler->getModule('openbusiness_content_creator')
        ->getPath() . '/content/images/' . $image_name;
    }
    else {
      $image_path = $this->moduleHandler->getModule('openbusiness_content_creator')
        ->getPath() . '/content/files/' . $image_name;
    }
    return $image_path;
  }

  /**
   * Imports contents.
   */
  public function createContent() {
    $this->importContentFromFile('menu', 'main');
    $this->importContentFromFile('term', 'tags');
    $this->importContentFromFile('menu', 'terms-privacy');
    $this->importContentFromFile('menu', 'social-links');
    $this->importContentFromFile('node', 'benefits');
    $this->importContentFromFile('node', 'hero_ct');
    $this->importContentFromFile('node', 'testimonials');
    $this->importContentFromFile('node', 'article');
    $this->importContentFromFile('node', 'portofolio');
    $this->importContentFromFile('node', 'page');
    $this->importContentFromFile('node', 'call_to_actions');
    $this->importContentFromFile('block', 'basic');
  }

  /**
   * Getting specific content from specific csv.
   */
  protected function importContentFromFile($entity_type, $bundle_machine_name) {
    $module_path = $this->getModulePath($entity_type, $bundle_machine_name);
    $data = array_map('str_getcsv', file($module_path));
    switch ($entity_type) {
      case 'node':
        $this->importNodes($data, $bundle_machine_name);
        break;

      case 'menu':
        $this->importMenus($data, $bundle_machine_name);
        break;

      case 'user':
        $this->importUsers($data, $bundle_machine_name);
        break;

      case 'block':
        $this->importBlocks($data, $bundle_machine_name);
        break;

      case 'term':
        $this->importTerms($data, $bundle_machine_name);
        break;
    }
  }

  /**
   * Function who imports nodes.
   */
  protected function importBlocks($data, $type) {
    foreach (array_slice($data, 1) as $line) {
      $generator = [
        'type' => $type,
        'info' => $line[1],
      ];
      if ($type === 'basic') {
        $add_body = [
          "body" => [
            'value' => $line[3],
            'format' => 'full_html',
          ],
        ];
        $generator += $add_body;
      }
    }
    $this->entityTypeManager->getStorage('block_content')
      ->create($generator)->save();
    $this->placeBlock($type, $line[1], $line[0], $line[2]);
  }

  /**
   * Function for placing block.
   */
  protected function placeBlock($type, $name, $region, $weight) {
    $block = $this->entityTypeManager->getStorage('block_content')
      ->loadByProperties(['type' => $type]);
    $id = array_keys($block);
    $generate = [
      'id' => strtolower(str_replace(" ", "_", $name)),
      'theme' => 'openbusiness_theme',
      'weight' => $weight,
      'status' => TRUE,
      'region' => $region,
      'plugin' => 'block_content:' . $block[$id[0]]->uuid(),
    ];

    if ($type === 'hero_image') {
      $visible = [
        'visibility' => [
          'request_path' => [
            'id' => 'request_path',
            'pages' => '<front>',
          ],
        ],
      ];
      $generate += $visible;
    }
    $this->entityTypeManager->getStorage('block')->create($generate)->save();
  }

  /**
   * Creating media with entityTypeManager.
   */
  protected function createMedia($imagePath) {
    $filename = basename($imagePath);
    $uri = $this->fileSystem->copy($imagePath, 'public://' . $filename, FileSystemInterface::EXISTS_REPLACE);
    $file = $this->entityTypeManager->getStorage('file')->create([
      'uri' => $uri,
      'status' => 1,
      'uid' => \Drupal::currentUser()->id(),
    ]);
    $file->save();
    $this->generateUuids([$file->uuid() => 'file']);
    return $file->id();
  }

  /**
   * Stores record of content entities created by this import.
   *
   * @param array $uuids
   *   Array of UUIDs where the key is the UUID and the value is the entity
   *   type.
   */
  protected function generateUuids(array $uuids) {
    $uuids = $this->state->get('content_creator_uuids', []) + $uuids;
    $this->state->set('content_creator_uuids', $uuids);
  }

  /**
   * Function who imports terms.
   */
  protected function importTerms($data, $type) {
    foreach (array_slice($data, 1) as $line) {
      $generator = [
        'name' => $line[0],
        'vid' => $type,
        'path' => [
          'alias' => '/blog/' . strtolower($line[0]),
        ],
      ];
      $this->entityTypeManager->getStorage('taxonomy_term')
        ->create($generator)->save();
    }
  }

  /**
   * Function who setting image.
   */
  protected function setImage($data) {
    $image = $this->getImagesPath($data);
    return $this->createMedia($image);
  }

  /**
   * Function who imports content from html files.
   */
  protected function importHtmlFromFile($file)
  {
      return file_get_contents($this->moduleHandler->getModule('openbusiness_content_creator')
        ->getPath() . '/content/html/' . $file);
  }

  /**
   * Function who imports nodes.
   */
  protected function importNodes($data, $type) {
    foreach (array_slice($data, 1) as $line) {
      $generator = [
        'type' => $type,
        'title' => $line[0],
        'uid' => 1,
      ];
      if ($type === 'page') {
        if ($line[2] == 1) {
          $settings_page = [
            'promote' => $line[2],
          ];
        }
        else {
          $path = explode(' ', trim($line[0]));
          $settings_page = [
            'path' => [
              'alias' => '/' . strtolower($path[0]),
            ],
          ];
        }
        $generator += $settings_page;
        if (!empty($line[3])) {
          $generator_para = $this->addParagraphToNode($line[3]);
          $generator += $generator_para;
        }
      }
      if ($type === 'article') {
        $term = $this->entityTypeManager->getStorage('taxonomy_term')
          ->loadByProperties(['name' => $line[3]]);
        $tags = [
          'field_tags' => [
            'target_id' => key($term),
          ],
        ];
        $generator += $tags;
        if (!empty($line[4])) {
          $generator_para = $this->addParagraphToNode($line[4]);
          $generator += $generator_para;
        }
      }
      if ($type === 'testimonials') {
        $field_role = [
          'field_role' => $line[1],
          'body' => [
            'value' => $line[2],
            'format' => 'full_html',
          ],
          'field_image' => [
            'target_id' => $this->setImage($line[3]),
            'alt' => $line[4],
          ],
        ];
        $generator += $field_role;
      }
      if ($type === 'benefits') {
        for ($i = 1; $i <= 8; $i++) {
          $generator['field_benefits_element_row'][$i - 1]['value'] = $line[$i];
        }
      }
      if ($type === 'hero_ct') {
        $add_image = [
          'field_landscape' => [
            'target_id' => $this->setImage($line[1]),
            'alt' => $line[2],
          ],
          'field_hero_title' => [
            'value' => $line[3],
            'format' => 'basic_html',
          ],
          'field_hero_text' => [
            'value' => $line[4],
            'format' => 'full_html',
          ],
          'field_text_alignment' => [
            'value' => 0,
          ],
          'field_hero_link' => [
            'uri' => 'internal:/' . $line[5],
            'title' => $line[6],
          ],
        ];
        $generator += $add_image;
      }
      if ($type === 'call_to_actions') {
        $add_image = [
          'field_call_image' => [
            'target_id' => $this->setImage($line[4]),
            'alt' => $line[5],
          ],
          'field_call_link' => [
            'uri' => 'internal:/' . $line[2],
            'title' => $line[3],
          ],
          "field_call_text" => [
            'value' => $line[1],
            'format' => 'full_html',
          ],
        ];
        $generator += $add_image;
      }
      if ($type === 'portofolio' || $type === 'article') {
        $field_image = [
          'field_image' => [
            'target_id' => $this->setImage($line[2]),
            'alt' => $line[0],
          ],
        ];
        $generator += $field_image;
        $path = str_replace(' ', '-', $line[0]);
        $path_alias = [
          'path' => [
            'alias' => '/' . $type . '/' . $path,
          ],
        ];
        $generator += $path_alias;
        if ($type === 'portofolio' && !empty($line[3])) {
          $generator_para = $this->addParagraphToNode($line[3]);
          $generator += $generator_para;
        }
      }
      if ($type === 'article' || $type === 'page' || $type === 'portofolio') {
        $get_body = $this->importHtmlFromFile($line[1]);
        $field_body = [
          'body' => [
            'value' => $get_body,
            'format' => 'full_html',
          ],
        ];
        $generator += $field_body;
      }
      $this->entityTypeManager->getStorage('node')->create($generator)->save();
    }
  }

  /**
   * Function which add paragraph fields to node.
   */
  protected function addParagraphToNode($file) {
    $pieces = explode(",", $file);
    foreach ($pieces as $paragraph_line) {
      $string = preg_replace('/[0-9]+/', '', $paragraph_line);
      $paragraph = $this->createParagraph($paragraph_line, $string);
      $array[] = [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ];
    }
    return [
      'field_element' => $array,
    ];
  }

  /**
   * Function which create paragraphs.
   */
  protected function createParagraph($origin, $edited) {
    $module_path = $this->getModulePath('paragraphs', $origin);
    $data = array_map('str_getcsv', file($module_path));
    switch ($edited) {
      case 'block_quote':
        foreach (array_slice($data, 1) as $line) {
          $paragraph = $this->entityTypeManager->getStorage('paragraph')
            ->create([
              'type' => 'block_quote',
              'field_body' => $line[0],
            ]);
          $paragraph->save();
        }
        break;

      case 'image_list':
        $paragraph = [
          'type' => 'image_list',
        ];
        $pieces = explode(",", $data[1][0]);
        $x = 1;
        foreach ($pieces as $line) {
          $array = [
            'field_image' . $x => [
              'target_id' => $this->setImage($line),
              'alt' => 'OpenBusiness Images',
            ],
          ];
          $x++;
          $paragraph += $array;
        }
        $paragraph = $this->entityTypeManager->getStorage('paragraph')
          ->create($paragraph);
        $paragraph->save();

        break;
        
        case 'hero_image_cta':
          $paragraph = [
            'type' => 'hero_image_cta',
          ];
          foreach (array_slice($data, 1) as $line) {
            $array = [
              'field_hero_title' => [
                'value' => $line[0],
                'format' => 'full_html',
              ],
              'field_hero_subtitle' => [
                'value' => $line[1],
                'format' => 'full_html',
              ],
              'field_hero_button_link' => [
                'uri' => 'internal:/' . $line[2],
                'title' => $line[3],
              ],
              'field_hero_cta_image' => [
                'target_id' => $this->setImage($line[4]),
                'alt' => 'OpenBusiness Images',
              ],
              'field_hero_cta_image_position' => [
                'value' => $line[6],
                'format' => 'full_html',
              ],
            ];
          }
          $paragraph += $array;
          $paragraph = $this->entityTypeManager->getStorage('paragraph')
            ->create($paragraph);
          $paragraph->save();
        break;

      case 'paragraph_with_body':
        $paragraph = $this->entityTypeManager->getStorage('paragraph')->create([
          'type' => 'paragraph_with_body',
          'field_title' => [
            'value' => $data[1][0],
            'format' => 'full_html',
          ],
          'field_body' => [
            'value' => $data[1][1],
            'format' => 'full_html',
          ],
        ]);
        $paragraph->save();

        break;

      case 'paragraph_with_image':
        $paragraph = [
          'type' => 'paragraph_with_image',
        ];
        foreach (array_slice($data, 1) as $line) {
          $array = [
            'field_image1' => [
              'target_id' => $this->setImage($line[0]),
              'alt' => 'OpenBusiness Images',
            ],
            'field_title' => [
              'value' => $line[1],
              'format' => 'full_html',
            ],
            'field_body' => [
              'value' => $line[2],
              'format' => 'full_html',
            ],
            'field_image_position' => [
              'value' => $line[3],
              'format' => 'full_html',
            ],
          ];
        }
        $paragraph += $array;
        $paragraph = $this->entityTypeManager->getStorage('paragraph')
          ->create($paragraph);
        $paragraph->save();

        break;
        case 'hero_image':
          $paragraph = [
            'type' => 'hero_image',
          ];
          foreach (array_slice($data, 1) as $line) {
            $array = [
              'field_image_display' => [
                'value' => $line[0],
                'format' => 'full_html',
              ],
              'field_full_size_image' => [
                'target_id' => $this->setImage($line[1]),
                'alt' => 'OpenBusiness Images',
              ],
              'field_hero_title' => [
                'value' => $line[3],
                'format' => 'full_html',
              ],
              'field_hero_subtitle' => [
                'value' => $line[4],
                'format' => 'full_html',
              ],
              'field_hero_align_title_subtitle' => [
                'value' => $line[5],
                'format' => 'full_html',
              ],
              'field_hero_button_link' => [
                'uri' => 'internal:/' . $line[5],
                'title' => $line[6],
              ],
            ];
          }
          $paragraph += $array;
          $paragraph = $this->entityTypeManager->getStorage('paragraph')
            ->create($paragraph);
          $paragraph->save();
  
          break;
      case 'carousel':
        $paragraph = [
          'type' => 'carousel',
        ];
        $pieces = explode(",", $data[1][0]);
        foreach ($pieces as $line) {
          $array[] = [
            'target_id' => $this->setImage($line),
            'alt' => 'OpenBusiness Images',
          ];
          $paragraph['field_image'] = $array;
        }
        $paragraph = $this->entityTypeManager->getStorage('paragraph')
          ->create($paragraph);
        $paragraph->save();

        break;

      case 'attachment':
        $paragraph = [
          'type' => 'attachment',
        ];
        $pieces = explode(",", $data[1][0]);
        foreach ($pieces as $line) {
          $array[] = [
            'target_id' => $this->setImage($line),
            'alt' => 'OpenBusiness Images',
          ];
          $paragraph['field_fiels'] = $array;
        }
        $paragraph = $this->entityTypeManager->getStorage('paragraph')
          ->create($paragraph);
        $paragraph->save();

        break;
    }
    return $paragraph;
  }

  /**
   * Function which imports menu links for the main menu.
   */
  protected function importMenus($data, $type) {
    $i = 0;
    // Links for the main menu.
    foreach (array_slice($data, 1) as $line) {
      if ($line[2] == 0) {
        $route = [
          'link' => [
            'uri' => $line[1],
          ],
        ];
      }
      else {
        $route = [
          'link' => [
            'uri' => 'internal:/' . $line[1],
          ],
        ];
      }
      $general = [
        'enabled' => 1,
        'title' => $line[0],
        'menu_name' => $type,
        'weight' => $i++,
      ];
      $general += $route;
      $this->entityTypeManager->getStorage('menu_link_content')
        ->create($general)
        ->save();
    }
  }

}
