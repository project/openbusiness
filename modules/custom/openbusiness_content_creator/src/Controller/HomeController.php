<?php

namespace Drupal\openbusiness_content_creator\Controller;

use \Symfony\Component\HttpFoundation\RedirectResponse;
use \Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;

/**
 * Returns a simple page.
 *
 * @return array
 *   Controller used to provide an empty route to serve as the homepage.
 */
class HomeController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function page() {

    /* Generates an empty page. */
    return [
      '#title' => 'Home',
    ];
  }

}
